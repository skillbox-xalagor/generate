#include <iostream>
#include <algorithm>
#include <vector>
#include <list>
#include <numeric>

int main()
{
	std::vector<int> v(10);
	std::generate(v.begin(), v.end(), [n = 0]() mutable { return n++; });
	std::cout << "v: ";
	for (auto i : v)
		std::cout << i << " ";
	std::cout << "\n";

	std::list<int> l(10);
	std::iota(l.begin(), l.end(), 0);
	std::transform(l.cbegin(), l.cend(), l.begin(), [](int i) { return i * 2; });
	std::cout << "l: ";
	for (auto i : l)
		std::cout << i << " ";
	std::cout << "\n";

	l.erase(std::remove_if(l.begin(), l.end(), [](int i) { return (i > 3); }), l.end());
	std::cout << "l: ";
	for (auto i : l)
		std::cout << i << " ";
	std::cout << "\n";
}
